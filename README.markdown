xkk2[9d
=======

This was a mistake

This is a small sound toy/experiemnt that I just felt like making. It's hosted on itch: https://chz.itch.io/xkk29d

When running in test mode (which happens automatically when you run this locally through a `file:` URL), this'll try to automatically load the contents of the `xkk2-9d/samples/` directory, which is expected to contain the sample pack from [Sample Pack Contest XIII](https://compo.beamsaber.com/spc13.php). For copyright reasons, I haven't actually included those samples in this repo. You can disable test mode by adding `?live` at the end of the URL.

Everything here is by me and released under MIT (see LICENSE.txt), except:

The [seedrandom library by David Bau](https://github.com/davidbau/seedrandom), released under MIT  (see `xkk2-9d/lib/seedrandom/LICENSE.md`)
