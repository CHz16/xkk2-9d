// emitter.js
// Struct for a sample emitter


import { emitterManager } from "./main.js";

import { randomIntInRange, randomFloatInRange, clamp } from "./numbers.js";


export class Emitter {
    constructor(id, name, coordContainer, rng, data) {
        function evaluateParameter(parameter, offset) {
            let t = 1;
            if (data[`${parameter}FarDistance`] != 0) {
                t = clamp(Math.abs(offset) / data[`${parameter}FarDistance`], 0, 1);
            }
            let min = data[`${parameter}MinNear`] + t * (data[`${parameter}MinFar`] - data[`${parameter}MinNear`]);
            let max = data[`${parameter}MaxNear`] + t * (data[`${parameter}MaxFar`] - data[`${parameter}MaxNear`]);
            return randomFloatInRange(min, max, rng);
        }

        this.id = id;
        this.name = name;
        if (data.sample == "(random)") {
            // Pull a random sample name from the sorted sample list to ensure identical enumeration order across browsers and sample list insertion order
            if (emitterManager.sortedSampleNames.length > 0) {
                let samples = emitterManager.sortedSampleNames;
                this.sample = samples[randomIntInRange(0, samples.length - 1, rng)];
            }
        } else {
            this.sample = data.sample;
        }

        this.isSpacer = ((Math.abs(id % 2) == 1) || this.sample === undefined || this.sample == "(none)");
        let offset = coordContainer.left ?? coordContainer.right;
        if (this.isSpacer) {
            this.width = evaluateParameter("emitterSpacing", offset);
        } else {
            this.width = evaluateParameter("emitterLength", offset);
        }
        if (coordContainer.left !== undefined) {
            this.left = coordContainer.left;
            this.right = coordContainer.left + this.width;
        } else {
            this.left = coordContainer.right - this.width;
            this.right = coordContainer.right;
        }

        let probability = evaluateParameter("emitterProbability", offset);
        if (this.isSpacer || randomFloatInRange(0, 1, rng) > probability) {
            this.inactive = true;
            return;
        }
        this.inactive = false;

        let sampleDuration = emitterManager.samples[this.sample].buffer.duration;
        this.loopLength = clamp(evaluateParameter("loopLength", offset), 0, sampleDuration);
        this.loopStart = randomFloatInRange(0, sampleDuration - this.loopLength, rng);
        this.loopEnd = this.loopStart + this.loopLength;
        this.startOffset = randomFloatInRange(0, this.loopLength, rng);

        this.playbackRate = evaluateParameter("playbackRate", offset);
    }
}
