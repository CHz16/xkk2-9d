// soundpreloader.js
// Object for easy preloading of sounds, with event hooks for actions to take
// place after loading.

// This class preloads the list of sounds given in the array sounds. Each
// element of this array represents the data of one sound effect and must be an
// object with the key property and either the urls or file property set:
//
//   - key: A key to refer to the sound.
//
//   - urls: An array of files to load. This array should consist of the same
//           sound effect in multiple formats for browser compatibility, so
//           maybe something like ["blah.ogg", "blah.wav"]. This class will load
//           the first file in the array that it guesses the browser can play
//           based on the file extension. If it doesn't think it can play any of
//           them, errorCallback will be called (see below).
//
//   - file: A File object to load the contents of.
//
// The second argument is an audio context to create the audio buffers in.
//
// The third optional argument is an object with any or all of the following
// properties set:
//
//   - base: A base directory for sound URLs. If this property is set, then the
//           sound at sounds[i].url[j] will be fetched from
//           (base + sounds[i].url[j]).
//
//   - loadCallback: A function to call after any sound is loaded. The function
//                   signature should look like this:
//                       function loadCallback(preloader, soundsHandled, index)
//                   preloader is the preloader object, soundsHandled is the
//                   total number of sounds that have loaded or errored, and
//                   index is the index of the sound in sounds that was just
//                   loaded. This function does not need a return value, but if
//                   it returns true, then no further sounds will be loaded.
//
//   - errorCallback: A function to call if there's an error in loading a sound.
//                    The function signature should look like this:
//                        function errorCallback(pl, soundsHandled, index)
//                    pl is the preloader object, soundsHandled is the total
//                    number of sounds that have loaded or errored, and index is
//                    the index of the sound in sounds that just failed to load.
//                    This function does not need a return value, but if it
//                    returns true, then no further sounds will be loaded.
//
//   - allCallback: A function to call after all sounds have either loaded or
//                  errored out. The function signature should look like this:
//                      function allCallback(preloader, soundsHandled, errors)
//                  preloader is the preloader object, soundsHandled is the
//                  total number of sounds that were attempted to be loaded, and
//                  errors is an array containing the keys of all the sounds
//                  that were unable to be loaded. This callback will be called
//                  AFTER loadCallback or errorCallback is called for the final
//                  sound to be loaded. If loadCallback or errorCallback returns
//                  true, this callback will be called with soundsHandled and
//                  errors counting only the sounds that were loaded. If the
//                  first argument is an empty array, this callback will be
//                  called immediately in the constructor.
//
//   - maxRequests: Maximum number of requests to have open at any one time.
//                  Defaults to 10.

const MIME_TYPE_FOR_EXTENSION = {
    "wav": "audio/wav",
    "WAV": "audio/wav",
    "mp3": "audio/mpeg",
    "MP3": "audio/mpeg",
    "ogg": "audio/ogg",
    "OGG": "audio/ogg"
};
Object.freeze(MIME_TYPE_FOR_EXTENSION);

export const SoundPreloadStatus = {unloaded: 0, loading: 1, loaded: 2, errored: 3};
Object.freeze(SoundPreloadStatus);


export class SoundPreloader {
    constructor(sounds, context, args = {}) {
        this.sounds = sounds;
        this.base = args.base ?? "";
        this.loadCallback = args.loadCallback ?? function(preloader, soundsHandled, index) {};
        this.errorCallback = args.errorCallback ?? function(preloader, soundsHandled, index) {};
        this.allCallback = args.allCallback ?? function(preloader, soundsHandled, errors) {};
        let maxRequests = args.maxRequests ?? 10;

        this.canPlayType = new Object();
        this.context = context;
        this.requests = new Object();
        this.fileTypes = new Object();
        this.soundBuffers = new Object();
        this.blobs = new Object();
        this.statuses = new Object();
        this.isLoading = false;
        this.soundsHandled = 0;
        this.errors = [];

        // First, we check what kinds of formats the browser can and can't play.
        let audioTester = new Audio();
        this.canPlayType["wav"] = (audioTester.canPlayType("audio/wav") !== "" && audioTester.canPlayType("audio/wav") !== "no");
        this.canPlayType["WAV"] = this.canPlayType["wav"];
        this.canPlayType["mp3"] = (audioTester.canPlayType("audio/mpeg") !== "" && audioTester.canPlayType("audio/mpeg") !== "no");
        this.canPlayType["MP3"] = this.canPlayType["mp3"];
        this.canPlayType["ogg"] = (audioTester.canPlayType("audio/ogg") !== "" && audioTester.canPlayType("audio/ogg") !== "no");
        this.canPlayType["OGG"] = this.canPlayType["ogg"];

        // Now we can kick off preloading.
        this.isLoading = true;
        if (sounds.length > 0) {
            this.nextFile = Math.min(sounds.length, maxRequests);
            for (let i = 0; i < this.nextFile; i++) {
                this.preloadSound(i);
            }
        } else {
            this.finishLoading();
        }
    }

    // private

    preloadSound(index) {
        if (this.sounds[index].urls !== undefined) {
            // Loading from urls property -- file URLs
            // Find the first sound file that the browser can play.
            let soundUrl = "", extension = "";
            for (let urlIndex = 0; urlIndex < this.sounds[index].urls.length; urlIndex++) {
                let url = this.sounds[index].urls[urlIndex];
                extension = url.substring(url.lastIndexOf(".") + 1);
                if (this.canPlayType[extension]) {
                    soundUrl = url;
                    break;
                }
            }

            // If the browser can't play any of them, then error.
            if (soundUrl === "") {
                this.soundErrored(index);
                return;
            }

            // Otherwise, kick off an AJAX request for the sound.
            let preloader = this;
            let request = new XMLHttpRequest();
            request.open("GET", this.base + soundUrl);
            request.responseType = "arraybuffer";
            request.onload = function(event) { preloader.soundLoaded(index, event); };
            request.onerror = function(event) { preloader.soundErrored(index, event); };
            this.requests[this.sounds[index].key] = request;
            this.fileTypes[this.sounds[index].key] = MIME_TYPE_FOR_EXTENSION[extension];
            this.statuses[this.sounds[index].key] = SoundPreloadStatus.loading;
            request.send();
        } else {
            // Loading from file property -- File object
            let preloader = this;
            let fileReader = new FileReader();
            fileReader.onload = function(event) { event.target.response = event.target.result; preloader.soundLoaded(index, event); };
            fileReader.onerror = function(event) { fileReader.abort(); preloader.soundErrored(index, event); };
            this.requests[this.sounds[index].key] = fileReader;
            this.fileTypes[this.sounds[index].key] = this.sounds[index].file.type;
            this.statuses[this.sounds[index].key] = SoundPreloadStatus.loading;
            fileReader.readAsArrayBuffer(this.sounds[index].file);
        }
    };

    soundLoaded(index, event) {
        let preloader = this;
        let blob = new Blob([event.target.response], { type: this.fileTypes[this.sounds[index].key] });
        this.context.decodeAudioData(event.target.response, function(buffer) {
            if (!preloader.isLoading) {
                return;
            }

            let key = preloader.sounds[index].key;
            preloader.soundBuffers[key] = buffer;
            preloader.blobs[key] = blob;
            preloader.statuses[key] = SoundPreloadStatus.loaded;
            preloader.soundsHandled += 1;

            let stopLoading = preloader.loadCallback(preloader, preloader.soundsHandled, index);
            if (stopLoading || preloader.soundsHandled == preloader.sounds.length) {
                preloader.finishLoading();
            } else if (preloader.nextFile < preloader.sounds.length) {
                preloader.preloadSound(preloader.nextFile);
                preloader.nextFile += 1;
            }
        }, function(errorEvent) {
            preloader.soundErrored(index, errorEvent);
        });
    }

    soundErrored(index, event) {
        let key = this.sounds[index].key;
        this.statuses[key] = SoundPreloadStatus.errored;
        this.errors[this.errors.length] = key;
        this.soundsHandled += 1;

        let stopLoading = this.errorCallback(this, this.soundsHandled, index);
        if (stopLoading || this.soundsHandled == this.sounds.length) {
            this.finishLoading();
        } else if (this.nextFile < this.sounds.length) {
            this.preloadSound(this.nextFile);
            this.nextFile += 1;
        }
    }

    finishLoading() {
        this.isLoading = false;
        for (let i = 0; i < this.sounds.length; i++) {
            let key = this.sounds[i].key;
            if (this.statuses[key] == SoundPreloadStatus.loading) {
                this.requests[key].abort();
                this.statuses[key] = SoundPreloadStatus.unloaded;
            }
        }
        this.allCallback(this, this.soundsHandled, this.errors);
    }
}
