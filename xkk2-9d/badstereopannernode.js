// badstereopannernode.js
// Half-baked implementation of StereoPannerNode for Safari
// <https://webaudio.github.io/web-audio-api/#stereopanner-algorithm>


// Q: Why didn't you just subclass GainNode or something?
// A: Because that doesn't actually work in Safari, you can't instantiate subclasses because it'll throw an "Illegal constructor" error.

// Q: Why are these two different classes?
// A: Because you can't check how many channels there are in a node's output, and the algorithm has different math depending on whether the input is mono or stereo.

// Q: This sucks
// A: I know! I hate it!


export class BadStereoPannerMonoInputNode {
    constructor(context) {
        this._pan = 0;

        this.leftGain = context.createGain();
        this.rightGain = context.createGain();
        this.merger = context.createChannelMerger(2);
        this.leftGain.connect(this.merger, 0, 0);
        this.rightGain.connect(this.merger, 0, 1);

        this.setGains();
    }

    destroy() {
        this.leftGain.disconnect();
        this.rightGain.disconnect();
        delete this.leftGain;
        delete this.rightGain;
        delete this.merger;
    }

    connectFrom(node) {
        node.connect(this.leftGain);
        node.connect(this.rightGain);
    }

    connect(node) {
        this.merger.connect(node);
    }

    disconnectFrom(node) {
        node.disconnect(this.leftGain);
        node.disconnect(this.rightGain);
    }

    disconnect() {
        this.merger.disconnect();
    }

    get pan() {
        return this._pan;
    }

    set pan(value) {
        this._pan = value;
        this.setGains();
    }

    // private

    setGains() {
        let x = (this._pan + 1) / 2;
        let y = x * Math.PI / 2;
        this.leftGain.gain.setValueAtTime(Math.cos(y), 0);
        this.rightGain.gain.setValueAtTime(Math.sin(y), 0);
    }
}


export class BadStereoPannerStereoInputNode {
    constructor(context) {
        this._pan = 0;

        this.splitter = context.createChannelSplitter(2);
        this.leftChannelLeftInputGain = context.createGain();
        this.leftChannelRightInputGain = context.createGain();
        this.rightChannelLeftInputGain = context.createGain();
        this.rightChannelRightInputGain = context.createGain();
        this.merger = context.createChannelMerger(2);

        this.splitter.connect(this.leftChannelLeftInputGain, 0);
        this.splitter.connect(this.leftChannelRightInputGain, 1);
        this.splitter.connect(this.rightChannelLeftInputGain, 0);
        this.splitter.connect(this.rightChannelRightInputGain, 1);
        this.leftChannelLeftInputGain.connect(this.merger, 0, 0);
        this.leftChannelRightInputGain.connect(this.merger, 0, 0);
        this.rightChannelLeftInputGain.connect(this.merger, 0, 1);
        this.rightChannelRightInputGain.connect(this.merger, 0, 1);

        this.setGains();
    }

    destroy() {
        this.splitter.disconnect();
        this.leftChannelLeftInputGain.disconnect();
        this.leftChannelRightInputGain.disconnect();
        this.rightChannelLeftInputGain.disconnect();
        this.rightChannelRightInputGain.disconnect();
        delete this.splitter;
        delete this.leftChannelLeftInputGain;
        delete this.leftChannelRightInputGain;
        delete this.rightChannelLeftInputGain;
        delete this.rightChannelRightInputGain;
        delete this.merger;
    }

    connectFrom(node) {
        node.connect(this.splitter);
    }

    connect(node) {
        this.merger.connect(node);
    }

    disconnectFrom(node) {
        node.disconnect(this.splitter);
    }

    disconnect() {
        this.merger.disconnect();
    }

    get pan() {
        return this._pan;
    }

    set pan(value) {
        this._pan = value;
        this.setGains();
    }

    // private

    setGains() {
        let x = (this._pan <= 0) ? (this._pan + 1) : this._pan;
        let y = x * Math.PI / 2;
        let gainL = Math.cos(y), gainR = Math.sin(y);

        if (this._pan <= 0) {
            this.leftChannelLeftInputGain.gain.setValueAtTime(1, 0);
            this.leftChannelRightInputGain.gain.setValueAtTime(gainL, 0);
            this.rightChannelLeftInputGain.gain.setValueAtTime(0, 0);
            this.rightChannelRightInputGain.gain.setValueAtTime(gainR, 0);
        } else {
            this.leftChannelLeftInputGain.gain.setValueAtTime(gainL, 0);
            this.leftChannelRightInputGain.gain.setValueAtTime(0, 0);
            this.rightChannelLeftInputGain.gain.setValueAtTime(gainR, 0);
            this.rightChannelRightInputGain.gain.setValueAtTime(1, 0);
        }
    }
}
