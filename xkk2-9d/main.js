// main.js
// UI setup.


import { NUMBER_OF_TRACKS, SPEED_FACTOR, VALID_PARAMETER_RANGES, WINDOW_WIDTH, TEST_MODE, MEDIARECORDER_AVAILABLE, recursiveFreeze } from "./globaldefinitions.js";
import { spc13_samples } from "./samples/index.js"; // it would be nice of conditional imports existed, so we only load this in test mode

import { Emitter } from "./emitter.js";
import { EmitterManager } from "./emittermanager.js";
import { JSUserDefaults } from "./jsuserdefaults.js";
import { SoundPreloader } from "./soundpreloader.js";
import { Track } from "./track.js";

import { randomIntInRange, randomFloatInRange, clamp } from "./numbers.js";


//
// PARAMETERS
//

const INPUT_DEFAULT_PAIRINGS = {
    "seedtext": { defaultName: "seed", member: "value" },
    "randomizeseedcheckbox": { defaultName: "randomizeSeed", member: "checked" },
    "speedslider": { defaultName: "speed", member: "value" },
    "automationselect": { defaultName: "automation", member: "selectedIndex" },
    "automationchangeintensityselect": { defaultName: "automationIntensity", member: "selectedIndex" },
    "automationchangefrequencyselect": { defaultName: "automationFrequency", member: "selectedIndex" },
    "automationdirectioncheckbox": { defaultName: "automationDirectionIncreasing", member: "checked" },
    "recordcheckbox": { defaultName: "record", member: "checked" },
    "volumeslider": { defaultName: "volume", member: "value" }
};
recursiveFreeze(INPUT_DEFAULT_PAIRINGS);

const HELP_SECTIONS = [
    { id: "quickstart", name: "Quick Start" },
    { id: "samples", name: "Loading/Managing Samples" },
    { id: "editingtracks", name: "Editing Tracks" },
    { id: "playback", name: "Playback" },
    { id: "seed", name: "Seeds" },
    { id: "recording", name: "Recording" },
    { id: "name", name: "You're Probably Wondering About the Name" },
    { id: "credits", name: "Credits/Legal" }
];
recursiveFreeze(HELP_SECTIONS);


//
// GLOBALS
//

let audioContext;
export let emitterManager;
let defaults = new JSUserDefaults("xkk2[9d", {
    "randomizeSeed": true,
    "speed": 0,
    "automation": 0,
    "automationIntensity": 0,
    "automationFrequency": 0,
    "automationDirectionIncreasing": true,
    "volume": 1
});

let fileDragEventCount = 0;

let trackData;
let currentlyOpenTrackIndex, currentlyOpenTrackData;
let copyTrackData = undefined;
let showedEditingWhilePlayingWarning = false;

let playing = false;
let updateRequestID = undefined, lastUpdateTime = undefined;
export let position = undefined;
let tracks = undefined;

let lastAutomationUpdateTime = undefined, automationRNG = undefined;

let currentlyOpenHelpTopicIndex = undefined;
let loadedHelpImages = false;

let recordingStartTime = undefined, recordedChunks = undefined;


//
// INITIALIZATION
//

if (TEST_MODE) {
    let testCSS = document.createElement("linK");
    testCSS.rel = "stylesheet";
    testCSS.type = "text/css";
    testCSS.href = "test.css";
    document.querySelector("head").appendChild(testCSS);

    let debugP = document.createElement("p");
    debugP.id = "debug";
    document.body.appendChild(debugP);
}

window.addEventListener("load", init);
function init() {
    // Note that this context will be suspended because we're creating it without taking user input; the start button will activate it later
    audioContext = new (window.AudioContext || window.webkitAudioContext)();
    emitterManager = new EmitterManager(audioContext);
    emitterManager.setVolume(defaults.get("volume"));

    if (!(trackData = defaults.get("trackData"))) {
        resetTrackData();
    }

    loadControlsData();
    createPlaybackEventHooks();
    createVisualizerEventHooks();
    createVisualizerTracks();
    createTrackEditEventHooks();
    createSampleListEventHooks();
    createHelpEventHooks();
    createHelpSectionLinks();
    updateParameterRangesInHelp();
    updateIfRecordingIsEnabled();
    createRecordingEventHooks();

    if (TEST_MODE) {
        loadSamples(spc13_samples);
    } else {
        loadSamples([]);
    }
}

function resetTrackData() {
    trackData = [];
    for (let i = 0; i < NUMBER_OF_TRACKS; i++) {
        trackData.push({
            enabled: false,
            sample: "(none)",
            loopLengthMinNear: 0.75,
            loopLengthMaxNear: 1.25,
            loopLengthMinFar: 0.01,
            loopLengthMaxFar: 1.25,
            loopLengthFarDistance: 100000,
            playbackRateMinNear: 1,
            playbackRateMaxNear: 1,
            playbackRateMinFar: 0.75,
            playbackRateMaxFar: 1.25,
            playbackRateFarDistance: 100000,
            emitterProbabilityMinNear: 1,
            emitterProbabilityMaxNear: 1,
            emitterProbabilityMinFar: 1,
            emitterProbabilityMaxFar: 1,
            emitterProbabilityFarDistance: 100000,
            emitterLengthMinNear: 75,
            emitterLengthMaxNear: 125,
            emitterLengthMinFar: 10,
            emitterLengthMaxFar: 150,
            emitterLengthFarDistance: 100000,
            emitterSpacingMinNear: 200,
            emitterSpacingMaxNear: 400,
            emitterSpacingMinFar: 0,
            emitterSpacingMaxFar: 400,
            emitterSpacingFarDistance: 100000
        });
    }
}


//
// DRAG EVENTS
//

// These events trigger on every child of the container, which is not ideal, since showing the modal dialog will immediately cause a dragleave on the triggering element and so on. So instead, this tracks enter & leave events and shows and hides the modal dialog based on whether the enter/leave events are balanced. This depends highly on enters happening before leaves (which experimentally they do in FF and Safari) and assuredly is not robust lmao

function enableDrags() {
    document.getElementById("maincontainer").addEventListener("dragenter", handleDragEnter);
    document.getElementById("maincontainer").addEventListener("dragover", handleDragOver);
    document.getElementById("maincontainer").addEventListener("drop", handleDrop);
    document.getElementById("maincontainer").addEventListener("dragleave", handleDragLeave);
}

function disableDrags() {
    document.getElementById("maincontainer").removeEventListener("dragenter", handleDragEnter);
    document.getElementById("maincontainer").removeEventListener("dragover", handleDragOver);
    document.getElementById("maincontainer").removeEventListener("drop", handleDrop);
    document.getElementById("maincontainer").removeEventListener("dragleave", handleDragLeave);
}

function handleDragEnter(event) {
    fileDragEventCount += 1;
    showModalDialog("drop to add files");
    event.preventDefault();
}

function handleDragOver(event) {
    // just swallow this to prevent the browser from doing anything to the file
    event.preventDefault();
}

function handleDrop(event) {
    fileDragEventCount = 0;
    hideModalDialog();
    event.preventDefault();

    let files = [];
    if (event.dataTransfer.items) {
        for (let i = 0; i < event.dataTransfer.items.length; i++) {
            if (event.dataTransfer.items[i].kind == "file") {
                files.push(event.dataTransfer.items[i].getAsFile());
            }
        }
    }
    loadSamples(files);
}

function handleDragLeave(event) {
    fileDragEventCount -= 1;
    if (fileDragEventCount <= 0) {
        fileDragEventCount = 0;
        hideModalDialog();
    }
    event.preventDefault();
}


//
// MESSAGES
//

function showModalDialog(message, dismissable=false) {
    document.getElementById("modaldialogcontainer").style.display = "flex";
    document.getElementById("modalmessage").textContent = message;

    if (dismissable) {
        document.getElementById("modaldialogclosebox").style.display = "block";
        document.getElementById("modaldialogcontainer").addEventListener("click", handleHideModalDialogEvent);
    } else {
        document.getElementById("modaldialogclosebox").style.display = "none";
        document.getElementById("modaldialogcontainer").removeEventListener("click", handleHideModalDialogEvent);
    }
}

function hideModalDialog() {
    document.getElementById("modaldialogcontainer").style.display = "none";
}

function handleHideModalDialogEvent(event) {
    if (event.target == document.getElementById("modaldialogcontainer") || event.target == document.getElementById("modaldialogclosebox")) {
        hideModalDialog();
    }
}


//
// PLAYBACK
//

function loadControlsData() {
    if (defaults.get("seed") === null) {
        randomizeSeed();
    }
    for (let id of Object.keys(INPUT_DEFAULT_PAIRINGS)) {
        document.getElementById(id)[INPUT_DEFAULT_PAIRINGS[id].member] = defaults.get(INPUT_DEFAULT_PAIRINGS[id].defaultName);
    }
}

function createPlaybackEventHooks() {
    for (let id of Object.keys(INPUT_DEFAULT_PAIRINGS)) {
        document.getElementById(id).addEventListener("input", function(event) {
            defaults.set(INPUT_DEFAULT_PAIRINGS[id].defaultName, event.target[INPUT_DEFAULT_PAIRINGS[id].member]);
        });
    }

    document.getElementById("speedslider").addEventListener("input", function(event) {
        document.getElementById("automationselect").selectedIndex = 0;
        document.getElementById("automationselect").dispatchEvent(new Event("input"));
    });

    document.getElementById("haltmovementbutton").addEventListener("click", function(event) {
        document.getElementById("automationselect").selectedIndex = 0;
        defaults.set("automation", 0);
        document.getElementById("speedslider").value = 0;
        document.getElementById("speedslider").dispatchEvent(new Event("input"));
        defaults.set("speed", 0);
    });

    document.getElementById("seedtext").addEventListener("input", function(event) {
        document.getElementById("randomizeseedcheckbox").checked = false;
    });

    document.getElementById("startstopbutton").addEventListener("click", function(event) {
        if (!playing) {
            for (let i = 0; i < NUMBER_OF_TRACKS; i++) {
                if (trackData[i].sample != "(none)" && trackData[i].sample != "(random)" && emitterManager.samples[trackData[i].sample] === undefined) {
                    showModalDialog(`Couldn't find track ${i + 1}'s sample: ${trackData[i].sample}`, true);
                    return;
                }
            }

            if (emitterManager.sortedSampleNames.length == 0) {
                showModalDialog("You don't have any samples loaded right now, just a heads up.", true);
            }

            event.target.textContent = "Stop Playback";
            document.getElementById("seedtext").disabled = true;
            document.getElementById("resettracksbutton").disabled = true;
            document.getElementById("visualizerpositionline").style.display = "block";

            if (document.getElementById("randomizeseedcheckbox").checked) {
                randomizeSeed();
            }

            audioContext.resume();
            if (MEDIARECORDER_AVAILABLE) {
                document.getElementById("recordcheckbox").disabled = true;
                if (document.getElementById("recordcheckbox").checked) {
                    emitterManager.startRecording(receiveRecorderData, finalizeRecording);
                    recordingStartTime = performance.now();
                    recordedChunks = [];
                    updateRecordingTime(performance.now());
                }
            }

            position = 0;
            tracks = [];
            for (let i = 0; i < NUMBER_OF_TRACKS; i++) {
                let trackID = `${document.getElementById("seedtext").value}-${i}-${trackData[i].sample}`;
                let track = new Track(trackID, trackData[i]);
                tracks.push(track);
                if (trackData[i].enabled) {
                    for (let emitter of track.emitters) {
                        emitterManager.updateEmitter(emitter);
                    }
                }
            }

            playing = true;
            disableDrags();
            document.getElementById("samplelistaddsamplesbutton").disabled = true;
            lastUpdateTime = performance.now();
            lastAutomationUpdateTime = lastUpdateTime;
            automationRNG = new Math.seedrandom(`${document.getElementById("seedtext").value}-rng`);
            updateRequestID = window.requestAnimationFrame(updatePlayback);
        } else {
            event.target.textContent = "Start Playback";
            document.getElementById("seedtext").disabled = false;
            document.getElementById("resettracksbutton").disabled = false;

            if (MEDIARECORDER_AVAILABLE) {
                document.getElementById("recordcheckbox").disabled = false;
                if (document.getElementById("recordcheckbox").checked) {
                    emitterManager.stopRecording();
                    showModalDialog("Finalizing recording");
                    document.getElementById("recordtimerrow").textContent = "\n";
                    // cleanup of recording variables handled by finalizeRecording()
                }
            }

            position = undefined;
            tracks = undefined;
            emitterManager.reset();
            document.getElementById("visualizerpositiontext").textContent = "";
            document.getElementById("visualizerpositionline").style.display = "none";

            for (let i = 0; i < NUMBER_OF_TRACKS; i++) {
                clearVisualizerTrack(i);
            }

            playing = false;
            if (recordingStartTime === undefined) {
                enableDrags();
            }
            document.getElementById("samplelistaddsamplesbutton").disabled = false;
            lastUpdateTime = undefined;
            lastAutomationUpdateTime = undefined;
            automationRNG = undefined;
            window.cancelAnimationFrame(updateRequestID);
            updateRequestID = undefined;
        }
    });

    document.getElementById("volumeslider").addEventListener("input", function(event) {
        emitterManager.setVolume(event.target.value);
    });
}

function updatePlayback(timestamp) {
    let dt = timestamp - lastUpdateTime;
    lastUpdateTime = timestamp;

    let dx = SPEED_FACTOR * dt * document.getElementById("speedslider").value;
    position += dx;
    document.getElementById("visualizerpositiontext").textContent = position.toFixed(1);

    for (let i = 0; i < NUMBER_OF_TRACKS; i++) {
        let track = tracks[i];
        for (let removedEmitter of track.shift(dx)) {
            emitterManager.removeEmitter(removedEmitter.name);
        }
        if (trackData[i].enabled) {
            for (let emitter of track.emitters) {
                emitterManager.updateEmitter(emitter);
            }
        }
        updateVisualizerTrack(i);
    }

    let automationChangeFrequencySelect = document.getElementById("automationchangefrequencyselect");
    if (timestamp > lastAutomationUpdateTime + parseInt(automationChangeFrequencySelect.options[automationChangeFrequencySelect.selectedIndex].value, 10)) {
        lastAutomationUpdateTime = timestamp;
        let intensity = parseFloat(document.getElementById("automationchangeintensityselect").value);
        let sign = document.getElementById("automationdirectioncheckbox").checked ? 1 : -1;

        let automationSelect = document.getElementById("automationselect");
        let automationType = automationSelect.options[automationSelect.selectedIndex].value;
        if (automationType == "linear") {
            let speedSlider = document.getElementById("speedslider");
            speedSlider.value = clamp(parseFloat(speedSlider.value) + sign * 0.1 * intensity, -1, 1);
        } else if (automationType == "startAndStop") {
            let speedSlider = document.getElementById("speedslider");
            if (speedSlider.value == 0) {
                speedSlider.value = sign * randomFloatInRange(0, intensity, automationRNG);
            } else {
                speedSlider.value = 0;
            }
        } else if (automationType == "monotonic") {
            document.getElementById("speedslider").value = sign * randomFloatInRange(0, intensity, automationRNG);
        } else if (automationType == "random") {
            document.getElementById("speedslider").value = randomFloatInRange(-intensity, intensity, automationRNG);
        }
    }

    if (recordingStartTime !== undefined) {
        updateRecordingTime(timestamp);
    }

    updateRequestID = window.requestAnimationFrame(updatePlayback);
}

function randomizeSeed() {
    let seed = randomIntInRange(0, Number.MAX_SAFE_INTEGER);
    document.getElementById("seedtext").value = seed;
    defaults.set("seed", seed);
}


//
// RECORDING
//

function updateIfRecordingIsEnabled() {
    if (MEDIARECORDER_AVAILABLE) {
        document.getElementById("helprecordingwarning").style.display = "none";
        document.getElementById("recordcheckboxrow").style.display = "block";
        document.getElementById("recordcheckbox").disabled = false;
        document.getElementById("recordtimerrow").style.display = "block";
        document.getElementById("recordtimerrow").textContent = "\n";
    }
}

function createRecordingEventHooks() {
    document.getElementById("recordingcontainer").addEventListener("click", function(event) {
        if (event.target == document.getElementById("recordingcontainer")) {
            hideRecording();
        }
    });
    document.getElementById("recordingclosebox").addEventListener("click", hideRecording);
}

function updateRecordingTime(timestamp) {
    let duration = timestamp - recordingStartTime;
    let minutes = Math.floor(duration / 60000);
    let seconds = Math.floor(duration / 1000) % 60;
    if (seconds < 10) {
        seconds = `0${seconds}`;
    }
    document.getElementById("recordtimerrow").textContent = `🔴 ${minutes}:${seconds}`;
}

function hideRecording() {
    document.getElementById("recordingcontainer").style.display = "none";
    document.getElementById("recordingaudio").pause();
}

function receiveRecorderData(data) {
    recordedChunks.push(data);
}

function finalizeRecording(mimeType, extension) {
    let blob = new Blob(recordedChunks, { type: mimeType });
    let dataURL = URL.createObjectURL(blob);
    document.getElementById("recordingaudio").src = dataURL;
    document.getElementById("recordingdownloadlink").href = dataURL;
    document.getElementById("recordingdownloadlink").download = `${document.getElementById("seedtext").value}.${extension}`;

    hideModalDialog();
    document.getElementById("recordingcontainer").style.display = "flex";

    recordingStartTime = undefined;
    recordedChunks = undefined;
    enableDrags();
}


//
// VISUALIZER
//

function createVisualizerEventHooks() {
    document.getElementById("visualizertoggleallcheckbox").addEventListener("input", function(event) {
        for (let i = 0; i < NUMBER_OF_TRACKS; i++) {
            let checkbox = document.getElementById(`visualizertrackcontrolcheckbox${i}`);
            if (!checkbox.disabled && checkbox.checked != event.target.checked) {
                checkbox.checked = event.target.checked;
                checkbox.dispatchEvent(new Event("change"));
            }
        }
    });

    document.getElementById("resettracksbutton").addEventListener("click", function(event) {
        if (confirm("Are you sure you want to delete all current track data?")) {
            resetTrackData();
            defaults.set("trackData", trackData);
            document.getElementById("visualizertoggleallcheckbox").checked = false;
            for (let i = 0; i < NUMBER_OF_TRACKS; i++) {
                document.getElementById(`visualizertrackcontrolcheckbox${i}`).checked = false;
                document.getElementById(`visualizertrackcontrolcheckbox${i}`).disabled = true;
                document.getElementById(`visualizertrackcontrollabel${i}`).textContent = "(none)";
            }
        }
    });
}

function createVisualizerTracks() {
    for (let i = 0; i < NUMBER_OF_TRACKS; i++) {
        let controls = document.createElement("div");
        controls.id = `visualizertrackcontrol${i}`;
        let checkbox = document.createElement("input");
        checkbox.id = `visualizertrackcontrolcheckbox${i}`;
        checkbox.type = "checkbox";
        checkbox.disabled = (trackData[i].sample == "(none)");
        checkbox.checked = trackData[i].enabled;
        controls.appendChild(checkbox);
        let label = document.createElement("span");
        label.id = `visualizertrackcontrollabel${i}`;
        label.textContent = trackData[i].sample;
        controls.appendChild(label);
        document.getElementById("visualizertrackcontrols").appendChild(controls);

        let track = document.createElement("div");
        track.id = `visualizertrack${i}`;
        document.getElementById("visualizertracks").appendChild(track);
    }
}

function updateVisualizerTrack(i) {
    let track = document.getElementById(`visualizertrack${i}`);

    // Instead of deleting every single box of the track and recreating them every redraw, instead what we'll do is flag every box that exists, go through each emitter and update its box or create a box for it, and then delete all the boxes that we didn't update (which means their emitters no longer exist).
    // This may or may not be better for performance, but it's definitely better for making the click events on the boxes work consistently, because boy did Safari not like trying to trigger a click event on something that was immediately deleted

    for (let i = 0; i < track.childNodes.length; i++) {
        track.childNodes[i].className = "unfound";
    }

    for (let emitter of tracks[i].emitters) {
        if (emitter.isSpacer) {
            continue;
        }

        let emitterDivID = `visualizertrack${i}box${emitter.id}`;
        let box;
        if (box = document.getElementById(emitterDivID)) {
            box.className = "";
        } else {
            box = document.createElement("div");
            box.id = emitterDivID;
            box.style.width = emitter.width + "px";
            box.textContent = emitter.loopStart.toFixed(1) + "-" + emitter.loopEnd.toFixed(1);
            if (trackData[i].sample == "(random)") {
                box.textContent += `\n${emitter.sample}`;
            }
            box.addEventListener("click", function(event) {
            showModalDialog(
                `Sample: ${emitter.sample}` +
                `\nLoop start: ${emitter.loopStart} seconds` +
                `\nLoop end: ${emitter.loopEnd} seconds` +
                `\nStart offset: ${emitter.startOffset}` +
                `\nPlayback rate: ${emitter.playbackRate}x`,
            true);
            });
            track.appendChild(box);
        }

        if (!trackData[i].enabled) {
            box.className = "disabled";
        }
        box.style.left = (emitter.left - position + WINDOW_WIDTH / 2) + "px";
    }

    let unfoundBoxes = track.getElementsByClassName("unfound");
    while (unfoundBoxes.length > 0) {
        track.removeChild(unfoundBoxes[0]);
    }
}

function clearVisualizerTrack(i) {
    let track = document.getElementById(`visualizertrack${i}`);
    while (track.childNodes.length > 0) {
        track.removeChild(track.firstChild);
    }
}


//
// EDITING TRACKS
//

function createTrackEditEventHooks() {
    for (let i = 0; i < NUMBER_OF_TRACKS; i++) {
        document.getElementById(`visualizertrackcontrol${i}`).addEventListener("click", function(event) { showTrackEditBox(i); });
        document.getElementById(`visualizertrackcontrolcheckbox${i}`).addEventListener("click", function(event) { event.stopPropagation(); });
        document.getElementById(`visualizertrackcontrolcheckbox${i}`).addEventListener("change", function(event) {
            trackData[i].enabled = event.target.checked;
            defaults.set("trackData", trackData);
            if (playing) {
                if (trackData[i].enabled) {
                    for (let emitter of tracks[i].emitters) {
                        emitterManager.updateEmitter(emitter);
                    }
                } else {
                    for (let emitter of tracks[i].emitters) {
                        emitterManager.removeEmitter(emitter.name);
                    }
                }
            }
        });
    }

    document.getElementById("trackeditparameterselect").addEventListener("input", function(event) {
        let parameter = event.target.options[event.target.selectedIndex].value;
        document.getElementById("trackeditparameterminnear").value = currentlyOpenTrackData[`${parameter}MinNear`];
        document.getElementById("trackeditparametermaxnear").value = currentlyOpenTrackData[`${parameter}MaxNear`];
        document.getElementById("trackeditparameterminfar").value = currentlyOpenTrackData[`${parameter}MinFar`];
        document.getElementById("trackeditparametermaxfar").value = currentlyOpenTrackData[`${parameter}MaxFar`];
        document.getElementById("trackeditparameterfardistance").value = currentlyOpenTrackData[`${parameter}FarDistance`];
        document.getElementById("parameterrangemin").textContent = VALID_PARAMETER_RANGES[parameter].min ?? "-∞";
        document.getElementById("parameterrangemax").textContent = VALID_PARAMETER_RANGES[parameter].max ?? "∞";
    });

    document.getElementById("trackeditsampleselect").addEventListener("input", function(event) {
        currentlyOpenTrackData["sample"] = event.target.options[event.target.selectedIndex].value;
        let sample = emitterManager.samples[event.target.options[event.target.selectedIndex].value];
        if (sample !== undefined) {
            document.getElementById("trackeditsampleduration").textContent = `Duration: ${sample.buffer.duration} seconds`;
        } else {
            document.getElementById("trackeditsampleduration").textContent = "Duration: N/A";
        }
    });

    document.getElementById("trackeditparameterminnear").addEventListener("input", function(event) { updateWorkingTrackData("MinNear", event.target); });
    document.getElementById("trackeditparametermaxnear").addEventListener("input", function(event) { updateWorkingTrackData("MaxNear", event.target); });
    document.getElementById("trackeditparameterminfar").addEventListener("input", function(event) { updateWorkingTrackData("MinFar", event.target); });
    document.getElementById("trackeditparametermaxfar").addEventListener("input", function(event) { updateWorkingTrackData("MaxFar", event.target); });
    document.getElementById("trackeditparameterfardistance").addEventListener("input", function(event) { updateWorkingTrackData("FarDistance", event.target); });

    document.getElementById("trackeditcopybutton").addEventListener("click", function(event) {
        copyTrackData = Object.assign({}, currentlyOpenTrackData);
        document.getElementById("trackeditpastebutton").disabled = false;
    });
    document.getElementById("trackeditpastebutton").addEventListener("click", function(event) {
        let enabled = currentlyOpenTrackData["enabled"];
        currentlyOpenTrackData = Object.assign({}, copyTrackData);
        currentlyOpenTrackData["enabled"] = enabled;
        refreshTrackEditData();
    });

    document.getElementById("trackeditcancelbutton").addEventListener("click", function(event) { closeTrackEditBox(); });
    document.getElementById("trackeditsavebutton").addEventListener("click", function(event) {
        let select = document.getElementById("trackeditsampleselect");
        document.getElementById(`visualizertrackcontrollabel${currentlyOpenTrackIndex}`).textContent = select.options[select.selectedIndex].value;

        let sampleIsSelected = (select.options[select.selectedIndex].value != "(none)");
        let checkbox = document.getElementById(`visualizertrackcontrolcheckbox${currentlyOpenTrackIndex}`);
        if (!sampleIsSelected) {
            checkbox.disabled = true;
            checkbox.checked = false;
            currentlyOpenTrackData["enabled"] = false;
        } else {
            checkbox.disabled = false;
            if (trackData[currentlyOpenTrackIndex].sample == "(none)") {
                currentlyOpenTrackData.enabled = true;
                checkbox.checked = true;
            }
        }

        revertInvalidTrackData();
        trackData[currentlyOpenTrackIndex] = currentlyOpenTrackData;
        defaults.set("trackData", trackData);
        closeTrackEditBox();
    });
}

function showTrackEditBox(i) {
    currentlyOpenTrackIndex = i;
    currentlyOpenTrackData = Object.assign({}, trackData[i]);
    refreshTrackEditData();
    document.getElementById("trackeditcontainer").style.display = "flex";
    document.getElementById("trackedittracknumber").textContent = i + 1;
    document.getElementById("trackeditsavebutton").disabled = playing;

    document.getElementById("maincontainer").tabIndex = 0;
    document.getElementById("maincontainer").focus();
    document.getElementById("maincontainer").addEventListener("keydown", handleReturnInTrackEditBox);

    if (playing && !showedEditingWhilePlayingWarning) {
        showedEditingWhilePlayingWarning = true;
        showModalDialog("You can view the data but you can't edit tracks while playback is going", true);
    }
}

function closeTrackEditBox() {
    document.getElementById("trackeditcontainer").style.display = "none";
    currentlyOpenTrackIndex = undefined;
    currentlyOpenTrackData = undefined;

    delete document.getElementById("maincontainer").tabIndex;
    document.getElementById("maincontainer").removeEventListener("keydown", handleReturnInTrackEditBox);
}

function handleReturnInTrackEditBox(event) {
    if (event.key == "Enter") {
        document.getElementById("trackeditsavebutton").dispatchEvent(new Event("click"));
        event.preventDefault();
    }
}

function refreshTrackEditData() {
    // i hate it
    let sampleSelect = document.getElementById("trackeditsampleselect");
    sampleSelect.selectedIndex = 0; // default value in case the saved sample isn't loaded
    for (let i = 0; i < sampleSelect.options.length; i++) {
        if (sampleSelect.options[i].value == currentlyOpenTrackData.sample) {
            sampleSelect.selectedIndex = i;
            break;
        }
    }

    document.getElementById("trackeditsampleselect").dispatchEvent(new Event("input"));
    document.getElementById("trackeditparameterselect").dispatchEvent(new Event("input"));
}

function updateWorkingTrackData(key, field) {
    let parameterSelect = document.getElementById("trackeditparameterselect");
    currentlyOpenTrackData[parameterSelect.options[parameterSelect.selectedIndex].value + key] = field.value;
}

function revertInvalidTrackData() {
    for (let parameter of ["loopLength", "playbackRate", "emitterProbability", "emitterLength", "emitterSpacing"]) {
        for (let field of ["MinNear", "MaxNear", "MinFar", "MaxFar", "FarDistance"]) {
            let parsedValue = parseFloat(currentlyOpenTrackData[parameter + field]);
            if (isNaN(parsedValue) || parsedValue != currentlyOpenTrackData[parameter + field]) {
                currentlyOpenTrackData[parameter + field] = trackData[currentlyOpenTrackIndex][parameter + field];
            }
            currentlyOpenTrackData[parameter + field] = parsedValue;
        }
    }

    for (let parameter of ["loopLength", "playbackRate", "emitterProbability", "emitterLength", "emitterSpacing"]) {
        if (VALID_PARAMETER_RANGES[parameter].min !== undefined && currentlyOpenTrackData[`${parameter}MinNear`] < VALID_PARAMETER_RANGES[parameter].min) {
            currentlyOpenTrackData[`${parameter}MinNear`] = trackData[currentlyOpenTrackIndex][`${parameter}MinNear`];
        }
        if (VALID_PARAMETER_RANGES[parameter].max !== undefined && currentlyOpenTrackData[`${parameter}MaxNear`] > VALID_PARAMETER_RANGES[parameter].max) {
            currentlyOpenTrackData[`${parameter}MaxNear`] = trackData[currentlyOpenTrackIndex][`${parameter}MaxNear`];
        }
        if (currentlyOpenTrackData[`${parameter}MinNear`] > currentlyOpenTrackData[`${parameter}MaxNear`]) {
            currentlyOpenTrackData[`${parameter}MinNear`] = trackData[currentlyOpenTrackIndex][`${parameter}MinNear`];
            currentlyOpenTrackData[`${parameter}MaxNear`] = trackData[currentlyOpenTrackIndex][`${parameter}MaxNear`];
        }
        if (VALID_PARAMETER_RANGES[parameter].min !== undefined && currentlyOpenTrackData[`${parameter}MinFar`] < VALID_PARAMETER_RANGES[parameter].min) {
            currentlyOpenTrackData[`${parameter}MinFar`] = trackData[currentlyOpenTrackIndex][`${parameter}MinFar`];
        }
        if (VALID_PARAMETER_RANGES[parameter].max !== undefined && currentlyOpenTrackData[`${parameter}MaxFar`] > VALID_PARAMETER_RANGES[parameter].max) {
            currentlyOpenTrackData[`${parameter}MaxFar`] = trackData[currentlyOpenTrackIndex][`${parameter}MaxFar`];
        }
        if (currentlyOpenTrackData[`${parameter}MinFar`] > currentlyOpenTrackData[`${parameter}MaxFar`]) {
            currentlyOpenTrackData[`${parameter}MinFar`] = trackData[currentlyOpenTrackIndex][`${parameter}MinFar`];
            currentlyOpenTrackData[`${parameter}MaxFar`] = trackData[currentlyOpenTrackIndex][`${parameter}MaxFar`];
        }
    };
}


//
// LOADING SAMPLES
//

function createSampleListEventHooks() {
    document.getElementById("showsamplelistbutton").addEventListener("click", function(event) {
        document.getElementById("samplelistcontainer").style.display = "flex";
        document.getElementById("samplelistdeleteselectedbutton").disabled = (document.getElementById("samplelistselect").selectedIndex == -1);
    });

    let closeSampleList = function(event) {
        document.getElementById("samplelistcontainer").style.display = "none";
        document.getElementById("samplelistplayer").pause();
    };
    document.getElementById("samplelistcontainer").addEventListener("click", function(event) {
        if (event.target == document.getElementById("samplelistcontainer")) {
            closeSampleList();
        }
    });
    document.getElementById("samplelistclosebox").addEventListener("click", closeSampleList);

    document.getElementById("samplelistpreviousbutton").addEventListener("click", function(event) {
        let sampleListSelect = document.getElementById("samplelistselect");
        sampleListSelect.selectedIndex = (sampleListSelect.selectedIndex - 1 + sampleListSelect.options.length) % sampleListSelect.options.length;
        sampleListSelect.dispatchEvent(new Event("input"));
    });
    document.getElementById("samplelistnextbutton").addEventListener("click", function(event) {
        let sampleListSelect = document.getElementById("samplelistselect");
        sampleListSelect.selectedIndex = (sampleListSelect.selectedIndex + 1) % sampleListSelect.options.length;
        sampleListSelect.dispatchEvent(new Event("input"));
        document.getElementById("samplelistdeleteselectedbutton").disabled = false;
    });
    document.getElementById("samplelistselect").addEventListener("input", function(event) {
        let selectedSample = event.target.options[event.target.selectedIndex].value;
        document.getElementById("samplelistplayer").src = URL.createObjectURL(emitterManager.samples[selectedSample].blob);
        document.getElementById("samplelistplayer").play().catch(error => {});
        document.getElementById("samplelistdeleteselectedbutton").disabled = false;
    });

    document.getElementById("samplelistaddsamplesbutton").addEventListener("click", function(event) {
        document.getElementById("samplelistfileinput").click(); // lmao is this really the only way to open a file picker
    });
    document.getElementById("samplelistfileinput").addEventListener("change", function(event) {
        let files = [];
        for (let i = 0; i < event.target.files.length; i++) {
            files.push(event.target.files[i]);
        }
        loadSamples(files);
    });

    document.getElementById("samplelistdeleteselectedbutton").addEventListener("click", function(event) {
        let sampleSelect = document.getElementById("samplelistselect");
        let selectedSample = sampleSelect.options[sampleSelect.selectedIndex].value;

        let inUseTracks = [], randomTracks = [];
        for (let i = 0; i < NUMBER_OF_TRACKS; i++) {
            if (trackData[i].sample == "(random)") {
                randomTracks.push(i);
            } else if (trackData[i].sample == selectedSample) {
                inUseTracks.push(i);
            }
        }

        if (playing && (inUseTracks.length > 0 || randomTracks.length > 0)) {
            let allTracks = inUseTracks.concat(randomTracks).sort();
            let joinedTracksString;
            if (allTracks.length == 1) {
                joinedTracksString = `track ${allTracks[0]}`;
            } else {
                let lastItem = allTracks.pop();
                joinedTracksString = "tracks " + allTracks.join(", ") + (allTracks.length > 1 ? "," : "") + " and " + lastItem;
            }
            showModalDialog(`The sample ${selectedSample} is currently in use by ${joinedTracksString} and can't be deleted while playback is going.`, true);
            return;
        }

        for (let inUseTrack of inUseTracks) {
            trackData[inUseTrack].enabled = false;
            trackData[inUseTrack].sample = "(none)";
            document.getElementById(`visualizertrackcontrolcheckbox${inUseTrack}`).disabled = true;
            document.getElementById(`visualizertrackcontrolcheckbox${inUseTrack}`).checked = false;
            document.getElementById(`visualizertrackcontrollabel${inUseTrack}`).textContent = "(none)";
        }
        defaults.set("trackData", trackData);

        emitterManager.deleteSample(selectedSample);
        let oldSelectedIndex = sampleSelect.selectedIndex;
        sampleSelect.removeChild(sampleSelect.options[oldSelectedIndex]);
        sampleSelect.selectedIndex = Math.min(oldSelectedIndex, sampleSelect.options.length - 1);
        updateSampleSelect(document.getElementById("trackeditsampleselect"), true);

        if (sampleSelect.options.length == 0) {
            event.target.disabled = true;
            document.getElementById("samplelistdeleteallbutton").disabled = true;
            document.getElementById("samplelistplayer").src = "";
        } else {
            sampleSelect.dispatchEvent(new Event("input"));
        }
    });

    document.getElementById("samplelistdeleteallbutton").addEventListener("click", function(event) {
        let inUseTracks = [], randomTracks = [];
        for (let i = 0; i < NUMBER_OF_TRACKS; i++) {
            if (trackData[i].sample == "(random)") {
                randomTracks.push(i);
            } else if (trackData[i].sample != "(none)") {
                inUseTracks.push(i);
            }
        }

        if (playing && (inUseTracks.length > 0 || randomTracks.length > 0)) {
            showModalDialog("Samples are currently in use and can't be deleted while playback is going.", true);
            return;
        }
        if (!confirm("Are you sure you want to delete all samples?")) {
            return;
        }

        for (let inUseTrack of inUseTracks) {
            trackData[inUseTrack].enabled = false;
            trackData[inUseTrack].sample = "(none)";
            document.getElementById(`visualizertrackcontrolcheckbox${inUseTrack}`).disabled = true;
            document.getElementById(`visualizertrackcontrolcheckbox${inUseTrack}`).checked = false;
            document.getElementById(`visualizertrackcontrollabel${inUseTrack}`).textContent = "(none)";
        }
        defaults.set("trackData", trackData);

        emitterManager.deleteAllSamples();
        updateSampleSelect(document.getElementById("samplelistselect"));
        updateSampleSelect(document.getElementById("trackeditsampleselect"), true);
        document.getElementById("samplelistplayer").src = "";
        document.getElementById("samplelistdeleteselectedbutton").disabled = true;
        document.getElementById("samplelistdeleteallbutton").disabled = true;
    });
}

function loadSamples(items) {
    disableDrags();

    let sounds = items.map(function(item) {
        if (typeof item == "string") {
            return { key: item, urls: [item] }
        } else {
            return { key: item.name, file: item }
        }
    });
    showModalDialog(`loading 0/${sounds.length}`);
    let soundPreloader = new SoundPreloader(sounds, audioContext, { base: "samples/", loadCallback: soundLoadErrorCallback, errorCallback: soundLoadErrorCallback, allCallback: soundsAllLoadedCallback });
}

function soundLoadErrorCallback(preloader, soundsHandled, index) {
    showModalDialog(`loading ${soundsHandled}/${preloader.sounds.length}`);
}

function soundsAllLoadedCallback(preloader, soundsHandled, errored) {
    emitterManager.addSamples(preloader.soundBuffers, preloader.blobs);
    if (Object.keys(preloader.soundBuffers).length > 0) {
        document.getElementById("samplelistdeleteallbutton").disabled = false;
    }

    updateSampleSelect(document.getElementById("samplelistselect"));
    updateSampleSelect(document.getElementById("trackeditsampleselect"), true);

    if (errored.length > 0) {
        showModalDialog("Failed to load: " + errored.join(", "), true);
    } else {
        hideModalDialog();
    }
    enableDrags();
}

function updateSampleSelect(select, includeNoneOption=false) {
    let savedSelectedIndex = select.selectedIndex, savedSelectedValue = (savedSelectedIndex != -1 ? select.options[savedSelectedIndex].value : undefined);
    while (select.childNodes.length > 0) {
        select.removeChild(select.firstChild);
    }
    if (includeNoneOption) {
        let noneOption = document.createElement("option");
        noneOption.value = "(none)";
        noneOption.text = "(none)";
        select.add(noneOption);
        let randomOption = document.createElement("option");
        randomOption.value = "(random)";
        randomOption.text = "(random)";
        select.add(randomOption);
    }
    for (let key of emitterManager.sortedSampleNames) {
        let option = document.createElement("option");
        option.value = key;
        option.text = key;
        select.add(option);
    }

    if (savedSelectedIndex != -1 && savedSelectedValue != select.options[savedSelectedIndex].value) {
        for (let i = 0; i < select.options.length; i++) {
            if (select.options[i].value == savedSelectedValue) {
                select.selectedIndex = i;
                break;
            }
        }
    } else {
        select.selectedIndex = savedSelectedIndex;
    }
}


//
// HELP
//

function createHelpEventHooks() {
    document.getElementById("showhelpbutton").addEventListener("click", function(event){
        if (currentlyOpenHelpTopicIndex === undefined) {
            if (!loadedHelpImages) {
                loadHelpImages();
            }
            showHelpIndex();
        } else {
            showHelpTopic();
        }
    });

    document.getElementById("helpindexcontainer").addEventListener("click", function(event) {
        if (event.target == document.getElementById("helpindexcontainer")) {
            hideHelpIndex();
        }
    });
    document.getElementById("helpindexclosebox").addEventListener("click", hideHelpIndex);

    document.getElementById("helptopiccontainer").addEventListener("click", function(event) {
        if (event.target == document.getElementById("helptopiccontainer")) {
            hideHelpTopic();
        }
    });
    document.getElementById("helptopicclosebox").addEventListener("click", hideHelpTopic);

    document.getElementById("helpbacklink").addEventListener("click", function(event) {
        event.preventDefault();
        hideHelpTopic();
        showHelpIndex();
    });

    document.getElementById("helptopicnavigationbacklink").addEventListener("click", function(event) {
        event.preventDefault();
        loadHelpTopic(currentlyOpenHelpTopicIndex - 1);
    });
    document.getElementById("helptopicnavigationindexlink").addEventListener("click", function(event) {
        event.preventDefault();
        hideHelpTopic();
        showHelpIndex();
    });
    document.getElementById("helptopicnavigationforwardlink").addEventListener("click", function(event) {
        event.preventDefault();
        loadHelpTopic(currentlyOpenHelpTopicIndex + 1);
    });
}

function createHelpSectionLinks() {
    // Table of contents
    for (let i = 0; i < HELP_SECTIONS.length; i++) {
        let a = document.createElement("a");
        a.textContent = HELP_SECTIONS[i].name;
        a.href = `#help-${HELP_SECTIONS[i].id}`;
        a.addEventListener("click", function(event) {
            event.preventDefault();
            hideHelpIndex();
            showHelpTopic();
            loadHelpTopic(i);
        });

        let li = document.createElement("li");
        li.appendChild(a);
        document.getElementById("helptableofcontents").appendChild(li);
    }
}

function loadHelpImages() {
    for (let image of document.getElementsByTagName("img")) {
        image.srcset = image.dataset["srcset"];
    }
    loadedHelpImages = true;
}

function updateParameterRangesInHelp() {
    // Automatically update the help text with the parameter ranges as they're defined, so that I don't inevitably forget to update the help if I change them
    // emitterProbability is not included here because that range should always be 0% to 100%. Right? I hope so. Please.
    for (let [parameter, unit] of [["loopLength", " second"], ["playbackRate", ""], ["emitterLength", " unit"], ["emitterSpacing", " unit"]]) {
        let elementId = `helptopic-${parameter.toLowerCase()}`
        if (VALID_PARAMETER_RANGES[parameter].min !== undefined) {
            let min = VALID_PARAMETER_RANGES[parameter].min;
            document.getElementById(`${elementId}min`).textContent = `${min}${unit}`;
            if (unit != "" && min != 1) {
                document.getElementById(`${elementId}min`).textContent += "s";
            }
        } else {
            document.getElementById(`${elementId}min`).textContent = "none";
        }
        if (VALID_PARAMETER_RANGES[parameter].max !== undefined) {
            let max = VALID_PARAMETER_RANGES[parameter].max;
            document.getElementById(`${elementId}max`).textContent = `${max}${unit}`;
            if (unit != "" && max != 1) {
                document.getElementById(`${elementId}max`).textContent += "s";
            }
        } else {
            document.getElementById(`${elementId}max`).textContent = "none";
        }
    }
}

function showHelpIndex() {
    document.getElementById("helpindexcontainer").style.display = "flex";
    unloadCurrentHelpTopic();
}

function hideHelpIndex() {
    document.getElementById("helpindexcontainer").style.display = "none";
}

function showHelpTopic() {
    document.getElementById("helptopiccontainer").style.display = "flex";
}

function hideHelpTopic() {
    document.getElementById("helptopiccontainer").style.display = "none";
}

function loadHelpTopic(index) {
    if (currentlyOpenHelpTopicIndex !== undefined) {
        unloadCurrentHelpTopic();
    }

    currentlyOpenHelpTopicIndex = index;
    document.getElementById("helptopicname").textContent = HELP_SECTIONS[index].name;
    document.getElementById("helptopicheader").textContent = HELP_SECTIONS[index].name;
    document.getElementById(`helptopic-${HELP_SECTIONS[index].id}`).style.display = "block";
    document.getElementById(`helptopic-${HELP_SECTIONS[index].id}`).parentNode.scrollTop = 0;

    if (index > 0) {
        document.getElementById("helptopicnavigationbacklink").style.display = "inline";
        document.getElementById("helptopicnavigationbacklink").textContent = `← ${HELP_SECTIONS[index - 1].name}`;
        document.getElementById("helptopicnavigationbacklink").href = `#help-${HELP_SECTIONS[index - 1].id}`;
    } else {
        document.getElementById("helptopicnavigationbacklink").style.display = "none";
    }

    if (index < HELP_SECTIONS.length - 1) {
        document.getElementById("helptopicnavigationforwardlink").style.display = "inline";
        document.getElementById("helptopicnavigationforwardlink").textContent = `${HELP_SECTIONS[index + 1].name} →`;
        document.getElementById("helptopicnavigationforwardlink").href = `#help-${HELP_SECTIONS[index + 1].id}`;
    } else {
        document.getElementById("helptopicnavigationforwardlink").style.display = "none";
    }

    // Hack -- only load the YouTube iframe in the name help topic once the user has actually opened that help topic
    if (HELP_SECTIONS[index].id == "name") {
        let iframe = document.getElementById("helpyoutubeiframe");
        if (iframe.src == "") {
            iframe.src = iframe.dataset["src"];
        }
    }
}

function unloadCurrentHelpTopic() {
    if (currentlyOpenHelpTopicIndex !== undefined) {
        document.getElementById(`helptopic-${HELP_SECTIONS[currentlyOpenHelpTopicIndex].id}`).style.display = "none";
    }
    currentlyOpenHelpTopicIndex = undefined;
}




function debug(s) {
    document.getElementById("debug").innerHTML += `<br />${s}`;
}
