// globaldefinitions.js
// Exported configuration constants


export function recursiveFreeze(obj) {
    for (let propertyName of Object.getOwnPropertyNames(obj)) {
        if (typeof obj[propertyName] == "object") {
            recursiveFreeze(obj[propertyName]);
        }
    }
    return Object.freeze(obj);
}


export const CONTAINER_WIDTH = 960, CONTAINER_HEIGHT = 540;
export const WINDOW_WIDTH = 490;
export const NUMBER_OF_TRACKS = 10;

export const VALID_PARAMETER_RANGES = {
    "loopLength": { min: 0.01 },
    "playbackRate": { min: 0.5, max: 2 },
    "emitterProbability": { min: 0, max: 1 },
    "emitterLength": { min: 10 },
    "emitterSpacing": { min: 0 }
};
recursiveFreeze(VALID_PARAMETER_RANGES);

export const SPEED_FACTOR = 5;

export const RECORDING_TIMESLICE = 1000; // milliseconds


// Calculated constants
export const TEST_MODE = (window.location.protocol == "file:") && ((new URL(window.location)).searchParams.get("live") === null);
export const MEDIARECORDER_AVAILABLE = (typeof MediaRecorder == "function");
