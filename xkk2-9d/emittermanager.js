// emittermanager.js
// Sample manager & player


import { WINDOW_WIDTH, RECORDING_TIMESLICE, MEDIARECORDER_AVAILABLE, recursiveFreeze } from "./globaldefinitions.js";
import { BadStereoPannerMonoInputNode, BadStereoPannerStereoInputNode } from "./badstereopannernode.js";

import { position } from "./main.js";


const MEDIARECORDER_TYPES = [
    { codecs: "audio/wav", mimeType: "audio/wav", extension: "wav" },
    { codecs: "audio/ogg;codecs=opus", mimeType: "audio/ogg", extension: "opus" },
    { codecs: "audio/webm;codecs=opus", mimeType: "audio/webm", extension: "webm" }
];
recursiveFreeze(MEDIARECORDER_TYPES);

const STEREOPANNERNODE_AVAILABLE = (typeof StereoPannerNode != "undefined");


export class EmitterManager {
    constructor(context) {
        this.context = context;
        this.samples = new Object();
        this.emitters = new Object();

        this.masterGain = this.context.createGain();
        this.setVolume(1);
        this.masterGain.connect(this.context.destination);

        this.masterCompressor = this.context.createDynamicsCompressor();
        this.masterCompressor.connect(this.masterGain);

        if (MEDIARECORDER_AVAILABLE) {
            for (let {codecs, mimeType, extension} of MEDIARECORDER_TYPES) {
                if (MediaRecorder.isTypeSupported(codecs)) {
                    this.streamDestination = this.context.createMediaStreamDestination();
                    this.masterCompressor.connect(this.streamDestination);
                    this.mediaRecorder = new MediaRecorder(this.streamDestination.stream, { mimeType: codecs });
                    this.recordingMimeType = mimeType;
                    this.recordingExtension = extension;
                    break;
                }
            }
        }

        this.startTime = undefined;

        this._sortedSampleNames = undefined;
    }

    addSamples(buffers, blobs) {
        for (let [key, value] of Object.entries(buffers)) {
            // If there's already a sample with the given name, then add/increment the number at the end
            let newKey = key, base, i;
            while (this.samples[newKey] !== undefined) {
                if (base === undefined) {
                    let components = key.split(" ");
                    let parsedLastComponent = parseInt(components[components.length - 1], 10);
                    if (!isNaN(parsedLastComponent) && components[components.length - 1] == parsedLastComponent) {
                        i = parsedLastComponent;
                        components.pop();
                    } else {
                        i = 1;
                    }
                    base = components.join(" ");
                }
                i += 1;
                newKey = `${base} ${i}`;
            }
            this.samples[newKey] = { buffer: value, blob: blobs[key] };
        }
        this._sortedSampleNames = undefined;
    }

    reset() {
        for (let emitterName of Object.keys(this.emitters)) {
            this.removeEmitter(emitterName);
        }
        this.startTime = undefined;
    }

    updateEmitter(emitter) {
        if (emitter.inactive) {
            return;
        }
        if (this.emitters[emitter.name] === undefined) {
            this.createEmitter(emitter);
        }

        let offset;
        if (emitter.left < position && emitter.right > position) {
            offset = 0;
        } else if (emitter.left > position) {
            offset = emitter.left - position;
        } else {
            offset = emitter.right - position;
        }
        offset = offset / (WINDOW_WIDTH / 2); // normalize to [-1, 1]
        if (STEREOPANNERNODE_AVAILABLE) {
            this.emitters[emitter.name].pannerNode.pan.setValueAtTime(offset, 0);
        } else {
            this.emitters[emitter.name].pannerNode.pan = offset;
        }
        this.emitters[emitter.name].gainNode.gain.setValueAtTime(1 - Math.abs(offset), 0);
        this.emitters[emitter.name].offset = offset;
    }

    removeEmitter(emitterName) {
        let emitter = this.emitters[emitterName];
        if (emitter === undefined) {
            return;
        }

        emitter.sourceNode.stop();
        emitter.sourceNode.disconnect();
        emitter.gainNode.disconnect();
        emitter.pannerNode.disconnect();
        if (!STEREOPANNERNODE_AVAILABLE) {
            emitter.pannerNode.destroy();
        }
        delete this.emitters[emitterName];
    }

    setVolume(volume) {
        this.masterGain.gain.setValueAtTime(volume, 0);
    }

    deleteSample(sample) {
        delete this.samples[sample];
        this._sortedSampleNames = undefined;
    }

    deleteAllSamples() {
        this.samples = new Object();
        this._sortedSampleNames = undefined;
    }

    get sortedSampleNames() {
        if (this._sortedSampleNames === undefined) {
            this._sortedSampleNames = Object.keys(this.samples).sort((a, b) => a.localeCompare(b));
        }
        return this._sortedSampleNames;
    }

    startRecording(dataAvailableCallback, stopCallback) {
        if (this.mediaRecorder === undefined) {
            throw new Error("MediaRecorder is not available in this browser");
        }

        this.mediaRecorder.ondataavailable = function(event) {
            dataAvailableCallback(event.data);
        };
        this.mediaRecorder.onstop = function(event) {
            stopCallback(this.recordingMimeType, this.recordingExtension);
        }.bind(this);
        this.mediaRecorder.start(RECORDING_TIMESLICE);
    }

    stopRecording() {
        if (this.mediaRecorder === undefined) {
            throw new Error("MediaRecorder is not available in this browser");
        }
        this.mediaRecorder.stop();
    }

    // private

    createEmitter(emitter) {
        let sourceNode = this.context.createBufferSource();
        let gainNode = this.context.createGain();

        let pannerNode;
        if (STEREOPANNERNODE_AVAILABLE) {
            pannerNode = this.context.createStereoPanner();
        } else {
            if (this.samples[emitter.sample].buffer.numberOfChannels == 1) {
                pannerNode = new BadStereoPannerMonoInputNode(this.context);
            } else {
                pannerNode = new BadStereoPannerStereoInputNode(this.context);
            }
        }
        this.emitters[emitter.name] = { sourceNode: sourceNode, gainNode: gainNode, pannerNode: pannerNode };

        sourceNode.buffer = this.samples[emitter.sample].buffer;
        sourceNode.loop = true;
        sourceNode.loopStart = emitter.loopStart;
        sourceNode.loopEnd = emitter.loopEnd;
        sourceNode.playbackRate.setValueAtTime(emitter.playbackRate, 0);

        sourceNode.connect(gainNode);
        if (STEREOPANNERNODE_AVAILABLE) {
            gainNode.connect(pannerNode);
        } else {
            pannerNode.connectFrom(gainNode);
        }
        pannerNode.connect(this.masterCompressor);

        if (this.startTime === undefined) {
            this.startTime = this.context.currentTime;
        }
        sourceNode.start(0, emitter.loopStart + (this.context.currentTime - this.startTime + emitter.startOffset) % emitter.loopLength);
    }
}
