// track.js
// Track encapsulation that creates managers based on given parameters


import { WINDOW_WIDTH } from "./globaldefinitions.js";

import { Emitter } from "./emitter.js";

import { randomIntInRange } from "./numbers.js";


export class Track {
    constructor(id, data) {
        this.id = id;
        this.data = Object.assign({}, data);
        if (this.data["sample"] == "(none)") {
            this.data["emitterProbabilityMinNear"] = 0;
            this.data["emitterProbabilityMaxNear"] = 0;
            this.data["emitterProbabilityMinFar"] = 0;
            this.data["emitterProbabilityMaxFar"] = 0;
            this.data["emitterSpacingMinNear"] = Number.MAX_SAFE_INTEGER / 2;
            this.data["emitterSpacingMaxNear"] = 0;
            this.data["emitterSpacingMinFar"] = Number.MAX_SAFE_INTEGER / 2;
            this.data["emitterSpacingMaxFar"] = 0;
        }
        this.left = -WINDOW_WIDTH / 2;

        let rng = new Math.seedrandom(this.id);
        this.emitters = [this.makeEmitter(0, {left: randomIntInRange(-WINDOW_WIDTH / 2, WINDOW_WIDTH / 2, rng)})];
        this.scrollEmittersLeft();
        this.scrollEmittersRight();
    }

    makeEmitter(id, coordContainer) {
        let name = `${this.id}-${id}`;
        let emitter = new Emitter(id, name, coordContainer, new Math.seedrandom(name), this.data);
        return emitter;
    }

    shift(dx) {
        this.left += dx;
        if (dx < 0) {
            return this.scrollEmittersLeft();
        } else if (dx > 0) {
            return this.scrollEmittersRight();
        } else {
            return [];
        }
    }

    scrollEmittersLeft() {
        let initialEmitterCount = this.emitters.length;

        while (this.emitters[0].left > this.left) {
            this.emitters.unshift(this.makeEmitter(this.emitters[0].id - 1, {right: this.emitters[0].left}));
        }

        let removed = [];
        while (this.emitters[this.emitters.length - 1].left > this.left + WINDOW_WIDTH) {
            if (removed.length < initialEmitterCount) {
                removed.push(this.emitters.pop());
            } else {
                this.emitters.pop();
            }
        }
        return removed;
    }

    scrollEmittersRight() {
        let initialEmitterCount = this.emitters.length;

        while (this.emitters[this.emitters.length - 1].right < this.left + WINDOW_WIDTH) {
            this.emitters.push(this.makeEmitter(this.emitters[this.emitters.length - 1].id + 1, {left: this.emitters[this.emitters.length - 1].right}));
        }

        let removed = [];
        while (this.emitters[0].right < this.left) {
            if (removed.length < initialEmitterCount) {
                removed.push(this.emitters.shift());
            } else {
                this.emitters.shift();
            }
        }
        return removed;
    }
}
